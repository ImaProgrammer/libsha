const BLOCK_INTS: usize = 16;
const BLOCK_BYTES: usize = BLOCK_INTS * 4;

// Generate a SHA1 using the input string
fn gen_sha1(input: String) {
    // SHA1 variables
    let mut h0: u32 = 0x6745_2301;
    let mut h1: u32 = 0xEFCD_AB89;
    let mut h2: u32 = 0x98BA_DCFE;
    let mut h3: u32 = 0x1032_5476;
    let mut h4: u32 = 0xC3D2_E1F0;
    // Other variables
    let mut msg = input.as_bytes().to_owned();
    let l = msg.len() as u64;
    
    msg.push(0x80);
    
}

// Generate a SHA2-256 using the input string
fn gen_sha2_256(input: &String) {
    // SHA2-256 variables
    let mut h0: u32 = 0x6A09_E667;
    let mut h1: u32 = 0xBB67_AE85;
    let mut h2: u32 = 0x3C6E_F372;
    let mut h3: u32 = 0xA54F_F53A;
    let mut h4: u32 = 0x510E_527F;
    let mut h5: u32 = 0x9B05_688C;
    let mut h6: u32 = 0x1F83_D9AB;
    let mut h7: u32 = 0x5BE0_CD19;
    
    let k = [0x428a_2f98, 0x7137_4491, 0xb5c0_fbcf, 0xe9b5_dba5, 0x3956_c25b, 0x59f1_11f1, 0x923f_82a4, 0xab1c_5ed5,
            0xd807_aa98, 0x1283_5b01, 0x2431_85be, 0x550c_7dc3, 0x72be_5d74, 0x80de_b1fe, 0x9bdc_06a7, 0xc19b_f174,
            0xe49b_69c1, 0xefbe_4786, 0x0fc1_9dc6, 0x240c_a1cc, 0x2de9_2c6f, 0x4a74_84aa, 0x5cb0_a9dc, 0x76f9_88da,
            0x983e_5152, 0xa831_c66d, 0xb003_27c8, 0xbf59_7fc7, 0xc6e0_0bf3, 0xd5a7_9147, 0x06ca_6351, 0x1429_2967,
            0x27b7_0a85, 0x2e1b_2138, 0x4d2c_6dfc, 0x5338_0d13, 0x650a_7354, 0x766a_0abb, 0x81c2_c92e, 0x9272_2c85,
            0xa2bf_e8a1, 0xa81a_664b, 0xc24b_8b70, 0xc76c_51a3, 0xd192_e819, 0xd699_0624, 0xf40e_3585, 0x106a_a070,
            0x19a4_c116, 0x1e37_6c08, 0x2748_774c, 0x34b0_bcb5, 0x391c_0cb3, 0x4ed8_aa4a, 0x5b9c_ca4f, 0x682e_6ff3,
            0x748f_82ee, 0x78a5_636f, 0x84c8_7814, 0x8cc7_0208, 0x90be_fffa, 0xa450_6ceb, 0xbef9_a3f7, 0xc671_78f2];

    let mut msg: ~[u8] = input.as_bytes().to_owned();
    let l = msg.len() as u64;

    msg.push(0x80);
}

